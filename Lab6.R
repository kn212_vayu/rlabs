
# Task 1
x <- c(1,3,6,8,9,10,11,12,14,16,17,18,20,22,23,24,25,27,29,30)
y <- c(5,9,15,19,21,23,25,27,31,35,37,39,43,47,49,51,53,57,61,63)

x <- c(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15)
y <- c(3, 6, 9, 13, 16, 19, 22, 26, 29, 32, 36, 39, 42, 45, 49)

cor(x, y) # Результат: 0.9996482

# Task 2
library(GGally)
library(ggplot2)
data <- data.frame(x, y)
ggpairs(data)

ggplot(data, aes(x = x, y = y)) +
  geom_point() +
  geom_smooth(method = "lm", se = FALSE, color = "red")

ggplot(data, aes(x = x, y = y)) +
  geom_bin2d(binwidth = c(1, 2)) +
  scale_fill_gradient(low = "white", high = "steelblue")

new_x <- c(4, 13, 19, 26)
predictions <- predict(lm(y ~ x, data), data.frame(x = new_x))
predictions

#Task 3
# Придумай два своих вектора, алгоритм тот же что и в 2 таске
