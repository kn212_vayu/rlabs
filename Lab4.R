library(modeest)
library(dplyr)

data(ChickWeight)

# Task 1
str(ChickWeight)

# Task 2
summary_stats <- function(x) {
  return(
         list(
              mean = mean(x), 
              median = median(x), 
              mode = mlv(x)
              )
         )
}

summary_stats(ChickWeight$weight)
summary_stats(ChickWeight$Time)
summary_stats(as.numeric(ChickWeight$Chick))

# Task 3
compute_stats <- function(var) {
  return (
          list(
               sd = sd(var),
               variation = (sd(ChickWeight$weight) / mean(ChickWeight$weight) * 100), 
               quantile = quantile(ChickWeight$weight),
               IQR = IQR(ChickWeight$weight)
               )
          )
}

compute_stats(ChickWeight$weight)
compute_stats(ChickWeight$Time)
compute_stats(as.numeric(as.character(ChickWeight$Chick)))

# Task 4
# Для створення нових змінних, які містять відношення двох змінних, 
# ми можемо використовувати операцію ділення. Наприклад, ми можемо створити нову змінну, 
# що містить відношення "вага курки" до "тривалість дієти"
ChickWeight$weight_duration_ratio <- ChickWeight$weight / ChickWeight$Time

# Task 5
# Plot the histogram
hist_data <- hist(ChickWeight$weight_duration_ratio, main = "Weight-Duration Ratio", xlab = "Weight/Time")
X <- ChickWeight$weight_duration_ratio
curve_data <- dnorm(hist_data$mids, mean = mean(X), sd = sd(X))
lines(hist_data$mids, curve_data*diff(hist_data$breaks)*hist_data$density[1], col = "red", lwd = 2)


# Task 6
boxplot(ChickWeight$weight, ChickWeight$Time, main = "Weight vs Time", xlab = "Time", ylab = "Weight")
# Task 7
top_10 <- ChickWeight %>%
  group_by(Chick) %>%
  summarise(weight_gain = max(weight) - min(weight)) %>%
  arrange(desc(weight_gain)) %>%
  head(10)
top_10
